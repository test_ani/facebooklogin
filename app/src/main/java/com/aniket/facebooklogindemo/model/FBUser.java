package com.aniket.facebooklogindemo.model;

import android.os.Parcel;
import android.os.Parcelable;

/**
 * Created by Aniket on 3/23/2018.
 */

public class FBUser implements Parcelable {

    private String profilePicURL;
    private String name;
    private String email;
    private String firstName;
    private String lastName;
    private String birthday;

    public FBUser() {
    }

    public FBUser(String profilePicURL, String name, String email, String firstName, String lastName, String birthday) {
        this.profilePicURL = profilePicURL;
        this.name = name;
        this.email = email;
        this.firstName = firstName;
        this.lastName = lastName;
        this.birthday = birthday;
    }

    public String getProfilePicURL() {
        return profilePicURL;
    }

    public void setProfilePicURL(String profilePicURL) {
        this.profilePicURL = profilePicURL;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getBirthday() {
        return birthday;
    }

    public void setBirthday(String birthday) {
        this.birthday = birthday;
    }

    private FBUser(Parcel in) {
        profilePicURL = in.readString();
        name = in.readString();
        email = in.readString();
        firstName = in.readString();
        lastName = in.readString();
        birthday = in.readString();
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(profilePicURL);
        dest.writeString(name);
        dest.writeString(email);
        dest.writeString(firstName);
        dest.writeString(lastName);
        dest.writeString(birthday);
    }

    @Override
    public int describeContents() {
        return 0;
    }

    public static final Creator<FBUser> CREATOR = new Creator<FBUser>() {
        @Override
        public FBUser createFromParcel(Parcel in) {
            return new FBUser(in);
        }

        @Override
        public FBUser[] newArray(int size) {
            return new FBUser[size];
        }
    };
}
