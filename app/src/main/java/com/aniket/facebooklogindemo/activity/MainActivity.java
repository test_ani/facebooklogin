package com.aniket.facebooklogindemo.activity;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.ImageView;
import android.widget.TextView;

import com.aniket.facebooklogindemo.R;
import com.aniket.facebooklogindemo.model.FBUser;
import com.squareup.picasso.Picasso;

public class MainActivity extends AppCompatActivity {

    private ImageView profilePicImageView;
    private TextView nameTextView;
    private TextView emailTextView;
    private TextView firstNameTextView;
    private TextView lastNameTextView;
    private TextView birthdayTextView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        bindView();
        getDataFromIntent();
    }

    private void bindView() {
        profilePicImageView = findViewById(R.id.iv_profile_pic);
        nameTextView = findViewById(R.id.tv_name);
        emailTextView = findViewById(R.id.tv_email);
        firstNameTextView = findViewById(R.id.tv_first_name);
        lastNameTextView = findViewById(R.id.tv_last_name);
        birthdayTextView = findViewById(R.id.tv_birthday);
    }

    private void getDataFromIntent() {
        Bundle data = getIntent().getExtras();
        if (data != null) {
            FBUser userDetails = data.getParcelable("Details");
            if (userDetails != null) {
                String profilePicURL = userDetails.getProfilePicURL();
                if (!profilePicURL.isEmpty()){
                    Picasso.with(this)
                            .load(profilePicURL)
                            .into(profilePicImageView);

                }

                String name = userDetails.getName();
                nameTextView.setText(name);

                String email = userDetails.getEmail();
                emailTextView.setText(email);

                String firstName = userDetails.getFirstName();
                firstNameTextView.setText(firstName);

                String lastName = userDetails.getLastName();
                lastNameTextView.setText(lastName);

                String birthday = userDetails.getBirthday();
                birthdayTextView.setText(birthday);
            }

        }

    }
}
