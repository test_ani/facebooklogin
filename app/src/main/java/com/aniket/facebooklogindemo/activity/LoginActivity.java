package com.aniket.facebooklogindemo.activity;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;

import com.aniket.facebooklogindemo.R;
import com.aniket.facebooklogindemo.model.FBUser;
import com.facebook.CallbackManager;
import com.facebook.FacebookCallback;
import com.facebook.FacebookException;
import com.facebook.FacebookSdk;
import com.facebook.GraphRequest;
import com.facebook.GraphResponse;
import com.facebook.Profile;
import com.facebook.login.LoginManager;
import com.facebook.login.LoginResult;
import com.facebook.login.widget.LoginButton;

import org.json.JSONObject;

import java.text.SimpleDateFormat;
import java.util.Arrays;
import java.util.Date;

public class LoginActivity extends AppCompatActivity {

    private static final String TAG = LoginActivity.class.getSimpleName();
    private LoginButton loginButton;

    private CallbackManager callbackManager;

    private static final String PUBLIC_PROFILE = "public_profile";
    private static final String EMAIL = "email";
    private static final String USER_FRIENDS = "user_friends";
    private static final String USER_BIRTHDAY = "user_birthday";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);
        // Facebook Init
        FacebookSdk.sdkInitialize(LoginActivity.this);
        callbackManager = CallbackManager.Factory.create();

        // Bind View
        loginButton = findViewById(R.id.login_button);
        loginButton.setReadPermissions(Arrays.asList(PUBLIC_PROFILE, EMAIL, USER_FRIENDS, USER_BIRTHDAY));

        try {
            LoginManager.getInstance().logOut();
        } catch (Exception ex) {
            ex.printStackTrace();
        }

        // Callback registration
        loginButton.registerCallback(callbackManager, new FacebookCallback<LoginResult>() {
            @Override
            public void onSuccess(LoginResult loginResult) {
                // App code
                GraphRequest request = GraphRequest.newMeRequest(
                        loginResult.getAccessToken(),
                        new GraphRequest.GraphJSONObjectCallback() {
                            @Override
                            public void onCompleted(JSONObject object, GraphResponse response) {
                                // Application code
                                try {
                                    Log.e(TAG, "Response " + response.toString());
                                    JSONObject fbJsonObject = response.getJSONObject();

                                    FBUser mFbUser = new FBUser();

                                    // Get FB Details
                                    String nameFB = "";
                                    if (fbJsonObject.has("name")) {
                                        nameFB = fbJsonObject.getString("name");
                                        mFbUser.setName(nameFB);
                                    } else
                                        nameFB = "";

                                    String fb_Id = "";
                                    if (fbJsonObject.has("id"))
                                        fb_Id = fbJsonObject.getString("id");
                                    else
                                        fb_Id = "";
//
                                    String firstNameFB = "";
                                    if (fbJsonObject.has("first_name")) {
                                        firstNameFB = fbJsonObject.getString("first_name");
                                        mFbUser.setFirstName(firstNameFB);
                                    } else
                                        firstNameFB = "";
//
                                    String lastNameFB = "";
                                    if (fbJsonObject.has("last_name")) {
                                        lastNameFB = fbJsonObject.getString("last_name");
                                        mFbUser.setLastName(lastNameFB);
                                    } else
                                        lastNameFB = "";

                                    Date date = null;
                                    String birthdayFB = "";
                                    if (fbJsonObject.has("birthday")) {
                                        String originalBirthdayFB = fbJsonObject.getString("birthday");
                                        // TODO: 6/16/2017 Convert From DD/MM/YYYY to yyyy-MM-dd
                                        SimpleDateFormat MMddyyyyDateFormatter = new SimpleDateFormat("MM/dd/yyyy");
                                        SimpleDateFormat yyyyMMddDateFormatter = new SimpleDateFormat("yyyy-MM-dd");
                                        date = MMddyyyyDateFormatter.parse(originalBirthdayFB);
                                        birthdayFB = yyyyMMddDateFormatter.format(date);
                                        mFbUser.setBirthday(originalBirthdayFB);
                                    } else
                                        birthdayFB = "";

                                    String facebookEmail = "";
                                    if (fbJsonObject.has("email")) {
                                        facebookEmail = fbJsonObject.getString("email");
                                        mFbUser.setEmail(facebookEmail);
                                    } else
                                        facebookEmail = "";

                                    String userFileFB = "";
                                    if (Profile.getCurrentProfile() != null) {
                                        userFileFB = "" + Profile.getCurrentProfile().getProfilePictureUri(200, 200);
                                        mFbUser.setProfilePicURL(userFileFB);
                                    } else
                                        userFileFB = "";

                                    Intent i = new Intent(LoginActivity.this, MainActivity.class);
                                    i.putExtra("Details", mFbUser);
                                    startActivity(i);
                                    finish();
//                                    checkFacebookLogin(facebookEmail, facebookEmail, "1", fb_Id, firstNameFB, lastNameFB, userFileFB);
                                } catch (Exception e) {
                                    e.printStackTrace();
                                }
                            }
                        });
                Bundle parameters = new Bundle();
                parameters.putString("fields", "id,name,email,first_name,last_name,birthday");
                request.setParameters(parameters);
                request.executeAsync();
            }

            @Override
            public void onCancel() {
                // App code
                Log.e(TAG, "onCancel");
            }

            @Override
            public void onError(FacebookException exception) {
                // App code
                Log.e(TAG, "exception" + exception);
                try {
                    LoginManager.getInstance().logOut();
                } catch (Exception ex) {
                    ex.printStackTrace();
                }
            }
        });

//        LoginManager.getInstance().registerCallback(callbackManager,
//                new FacebookCallback<LoginResult>() {
//                    @Override
//                    public void onSuccess(LoginResult loginResult) {
//                        // App code
//                    }
//
//                    @Override
//                    public void onCancel() {
//                        // App code
//                    }
//
//                    @Override
//                    public void onError(FacebookException exception) {
//                        // App code
//                    }
//                });

    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        callbackManager.onActivityResult(requestCode, resultCode, data);
        super.onActivityResult(requestCode, resultCode, data);
    }
}
